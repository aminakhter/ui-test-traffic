// var controller = new ScrollMagic.Controller();
// var tl = new TimelineMax();
// var controller_1 = new ScrollMagic.Controller();
// var tl_1 = new TimelineMax();

// // Section One
// var tween0 = tl.fromTo($('#section1'), 1, { opacity:0.3 }, { opacity:1 }, 0);
// var tween1 = tl.to($('#section1 .number'), 1, { y: 100, opacity:0, ease: Linear.easeNone }, 0.5);
// var tween2 = tl.to($('#section1 .img2'), 2, { y: 100, opacity:0, ease: Linear.easeNone }, 1);
// var tween3 = tl.to($('#section1 .overlay-img'), 1, { y: -80, opacity:0, ease: Linear.easeNone }, 1.5);
// var tween4 = tl.to($('#section1 h3, #section1 h2, #section1 p'), 1, { y: -80, ease: Linear.easeNone }, 1.2);

// tl.add(tween0).add(tween1).add(tween2).add(tween3);
// var scene = new ScrollMagic.Scene({
//     triggerElement: '#section1',
//     triggerHook: 0.5,
//     duration: $('#section1').outerHeight(),
//     offset:-10
// })
// .setTween(tl)
// .addIndicators({
//     colorTrigger: "white",
//     colorStart: "white",
//     colorEnd: "white",
//     indent: 0
//  })
// .addTo(controller);

// // Section 2
// var s2_0 = tl_1.fromTo($('#section2'), 1, { opacity:0.3 }, { opacity:1 }, 0);
// var s2_1 = tl_1.to($('#section2 .number'), 1, { y: 100, opacity:0, ease: Linear.easeNone }, 0.5);
// var s2_2 = tl_1.to($('#section2 .img2'), 2, { y: 100, opacity:0, ease: Linear.easeNone }, 1);
// var s2_3 = tl_1.to($('#section2 .overlay-img'), 1, { y: -80, opacity:0, ease: Linear.easeNone }, 1.5);
// var s2_4 = tl_1.to($('#section2 h3, #section2 h2, #section2 p'), 1, { y: -80, ease: Linear.easeNone }, 1.2);

// tl_1.add(s2_0).add(s2_1).add(s2_2).add(s2_3).add(s2_4);
// var scene_1 = new ScrollMagic.Scene({
//     triggerElement: '#section2',
//     triggerHook: 0.5,
//     duration: $('#section2').outerHeight(),
//     offset:-10
// })
// .setTween(tl_1)
// .addIndicators({
//     colorTrigger: "white",
//     colorStart: "white",
//     colorEnd: "white",
//     indent: 0
//  })
// .addTo(controller_1);

var controller = new ScrollMagic.Controller();
var tl = new TimelineMax();
var tlBody = new TimelineMax();

tlBody.to(".fixbg", 1, {top:"25%"}, 0);
var scene = new ScrollMagic.Scene({
    triggerElement: "body",
    triggerHook: 0,
    duration: $('body').outerHeight(),
    offset:100
})
.setTween([tlBody, TweenMax.to('.header__progress', 1, {width: '100%'})])
// .addIndicators({
//     colorTrigger: "yellow",
//     colorStart: "yellow",
//     colorEnd: "yellow",
//     indent: 0
//   })
.addTo(controller);

tl.fromTo(".main", 1, { backgroundPosition:"right 100%, right 0px" }, { backgroundPosition:"right 95%, right 100px", opacity:0 }, 3);
var scene = new ScrollMagic.Scene({
    triggerElement: ".main",
    triggerHook: 0.4,
    duration: $('.main').outerHeight(),
    offset:100
})
.setTween(tl) // trigger a TweenMax.to tween
// .addIndicators({
//     colorTrigger: "red",
//     colorStart: "red",
//     colorEnd: "red",
//     indent: 0
//   })
.addTo(controller);


$(".sec").each(function() {
    var tl = new TimelineMax();
    var section = $(this);
    var number = $(this).find('.number');
    var img1 = $(this).find('.img2');
    var img2 = $(this).find('.overlay-img, .overlay-img-r');
    var text = $(this).find('h3, h2, p');
    
    var t0 = tl.fromTo(section, 1, { opacity:0 }, { opacity:1 }, 0);
    var t1 = tl.to(number, 1, { y: 100, opacity:0, ease: Linear.easeNone }, 0.5);
    var t2 = tl.to(img1, 1, { y: 100, opacity:0, ease: Linear.easeNone }, 1.2);
    var t3 = tl.to(img2, 1, { y: -80, opacity:0, ease: Linear.easeNone }, 1);
    var t4 = tl.to(text, 1, { y: -80, ease: Linear.easeNone }, 1.4);


  tl.add(t0).add(t1).add(t2).add(t3).add(t4);

  var scene = new ScrollMagic.Scene({
    triggerElement: this,
    triggerHook: 0.4,
    duration: $(this).outerHeight(),
    offset:-10
  })
    .setTween(tl)
    // .addIndicators({
    //   colorTrigger: "white",
    //   colorStart: "white",
    //   colorEnd: "white",
    //   indent: 0
    // })
    .addTo(controller);
});